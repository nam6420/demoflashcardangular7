import { Component, OnInit } from '@angular/core';
import { faPenFancy, faVolumeUp, faFileAlt, faGraduationCap, faThLarge, faWindowRestore, faMeteor } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  faPenFancy = faPenFancy;
  faVolumeUp= faVolumeUp;
  faFileAlt= faFileAlt;
  faGraduationCap = faGraduationCap;
  faWindowRestore = faWindowRestore;
  faThLarge = faThLarge;
  faMeteor = faMeteor;

  constructor() { }

  ngOnInit(): void {
  }

}
