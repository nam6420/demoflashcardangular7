import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataServiceService implements InMemoryDbService {
  createDb() {
    const card = [
      { id: 1, name: 'Angular', content: "Demo Angular" },
      { id: 2, name: 'Javar Core', content: "Demo Java core" },
      { id: 3, name: 'Java Spring', content: "Demo Java Spring" },
      { id: 4, name: 'Java Script', content:  "Demo Java Script"},
      { id: 5, name: '日本語', content: "日本語を勉強したことがある" },
      { id: 6, name: '会社', content: "今からSMACVNで働いてる"},
      { id: 7, name: 'ふるさと', content: "僕のふるさとから来た" },
      { id: 8, name: 'おはよう', content: "「おはよう」は日本人の挨拶だ" },
    
    ];
    return {card};
  }
  
}
