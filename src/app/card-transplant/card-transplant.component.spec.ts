import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTransplantComponent } from './card-transplant.component';

describe('CardTransplantComponent', () => {
  let component: CardTransplantComponent;
  let fixture: ComponentFixture<CardTransplantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTransplantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTransplantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
