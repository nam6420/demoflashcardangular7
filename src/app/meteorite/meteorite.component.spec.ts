import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteoriteComponent } from './meteorite.component';

describe('MeteoriteComponent', () => {
  let component: MeteoriteComponent;
  let fixture: ComponentFixture<MeteoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeteoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
