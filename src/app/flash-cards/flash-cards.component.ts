import { Component, OnInit } from '@angular/core';
import { FlashcardService } from '../flashcard.service'
import { Cards } from '../cards';
import { faArrowLeft, faArrowRight, faKeyboard, faExpand } from '@fortawesome/free-solid-svg-icons';
import { trigger, state, style, transition, animate, query, group } from '@angular/animations';

const left = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(-100%)' }), animate('.3s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.3s ease-out', style({ transform: 'translateX(100%)' }))], {
      optional: true,
    }),
  ]),
];

const right = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(100%)' }), animate('.3s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.3s ease-out', style({ transform: 'translateX(-100%)' }))], {
      optional: true,
    }),
  ]),
];

@Component({
  selector: 'app-flash-cards',
  templateUrl: './flash-cards.component.html',
  styleUrls: ['./flash-cards.component.css'],
  
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateX(180deg)'
      })),
      state('inactive', style({
        transform: 'rotateX(0)'
      })),
      transition('active => inactive', animate('400ms ease-out')),
      transition('inactive => active', animate('400ms ease-in'))
    ]),
    trigger('animSlider', [
      transition(':increment', right),
      transition(':decrement', left),
    ]),
  ]
})
export class FlashCardsComponent implements OnInit {
  faArrowLeft= faArrowLeft;
  faArrowRight= faArrowRight;
  faKeyboard= faKeyboard;
  faExpand= faExpand;
  flip: string = 'inactive';
  slider: string = 'increment';

  card: Cards[];
  currentIndex: number;
  countIndex: number = 0;
  currentCardName: string;
  currentCardContent: string;

  isShowingCardName: boolean = true;
  isShowingCardContent: boolean = false;

  constructor(
    private flashcardService: FlashcardService,
  ) { }

  ngOnInit(): void {
    this.currentIndex = 1;
    this.getCards();
  }

  getCards(): void {
    this.flashcardService.getCards()
      .subscribe(card => {
        this.card = card;
        this.countIndex = card.length;
        this.getCurrentCard();
      })
  }

  getCurrentCard() {
    this.currentCardName = this.card[this.currentIndex].name;
    this.currentCardContent = this.card[this.currentIndex].content;
  }

  prev() {
    if (this.currentIndex === 1) {
      this.currentIndex = this.countIndex;
    } else {
      this.currentIndex--;
    }

    this.getCurrentCard();
    this.isShowingCardName = true;
    this.isShowingCardContent = false;
  }

  next() {
    if (this.currentIndex === this.countIndex) {
      this.currentIndex = 1;
    } else {
      this.currentIndex++;
    }

    this.getCurrentCard();
    this.isShowingCardName = true;
    this.isShowingCardContent = false;
  }

  onclick() {
    if (this.isShowingCardName) {
      this.isShowingCardName = false;
      this.isShowingCardContent = true;
    } else {
      this.isShowingCardContent = false;
      this.isShowingCardName = true;
    }
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
    
  }

}
