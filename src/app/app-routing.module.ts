import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FlashCardsComponent } from './flash-cards/flash-cards.component'
import { HomeComponent } from './home/home.component';
import { NavBarComponent } from './Share/nav-bar/nav-bar.component';
import { StudyComponent } from './study/study.component';
import { WriteComponent } from './write/write.component';
import { SpellComponent } from './spell/spell.component';
import{ Test1Component } from './test1/test1.component';
import { CardTransplantComponent } from './card-transplant/card-transplant.component';
import { MeteoriteComponent } from './meteorite/meteorite.component';

const routes: Routes = [
  { path: '', redirectTo: '/flash-cards', pathMatch: 'full' },
  { path: 'app-home', component: HomeComponent},
  { path: 'flash-cards', component: FlashCardsComponent },
  { path: 'app-nav-bar', component: NavBarComponent},
  { path: 'app-student', component: StudyComponent},
  { path: 'app-write', component: WriteComponent},
  { path: 'app-spell', component: SpellComponent},
  { path: 'app-test1', component: Test1Component},
  { path: 'app-card-transplant', component: CardTransplantComponent},
  { path: 'app-meteorite', component: MeteoriteComponent}
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
