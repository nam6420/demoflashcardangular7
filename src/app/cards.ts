export interface Cards {
    id: number;
    name: string;
    content: string;
}
