import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlashCardsComponent } from './flash-cards/flash-cards.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import { NavBarComponent } from './Share/nav-bar/nav-bar.component';
import { FooterComponent } from './Share/footer/footer.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataServiceService } from './in-memory-data-service.service';
import { HomeComponent } from './home/home.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { StudyComponent } from './study/study.component';
import { WriteComponent } from './write/write.component';
import { SpellComponent } from './spell/spell.component';
import { Test1Component } from './test1/test1.component';
import { CardTransplantComponent } from './card-transplant/card-transplant.component';
import { MeteoriteComponent } from './meteorite/meteorite.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons'; 


@NgModule({
  declarations: [	
    AppComponent,  
    FlashCardsComponent, NavBarComponent, FooterComponent, HomeComponent, SideBarComponent, StudyComponent, WriteComponent, SpellComponent, Test1Component, CardTransplantComponent, MeteoriteComponent,
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    MatSliderModule,
    MatCardModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataServiceService),
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
    library.add(fas, far, fab);
  }
}
